import React from 'react'
import { Form, Input, Modal } from 'antd'
import { createFolder, createTask } from './hooks'
import { getFolders } from '../../Layout/hooks'
import { getTasksByFolder } from '../../pages/client/Folder/hooks'

const CreateModal = (props) => {
    const { visible, onToggle, folder } = props
    
    const [form] = Form.useForm()
    const [title, setTitle] = React.useState(null)

    const handleCloseModal = () => onToggle(false)

    const handleChangeTitle = (e) => setTitle(e.target.value) 

    const handleCreateFolder = () => {
        form.validateFields().then(() => {
            if (folder) {
                createFolder(title).then(() => {
                    getFolders()
                })
            } else {
                createTask(title).then(() => {
                    getTasksByFolder()
                })
            }
            onToggle(false)
        })
    }

    return (
        <Modal title={`Создание ${folder ? 'папки' : 'задачи'}`} visible={visible} onOk={handleCreateFolder} onCancel={handleCloseModal} okText="Создать" cancelText="Отмена">
            <Form form={form}>
                <div>{`Введите название ${folder ? 'папки': 'задачи'}:`}</div>
                <Form.Item name="title" rules={[{ required: true, message: `Пожалуйста, ведите название ${folder ? 'папки': 'задачи'}` }]}>
                    <Input value={title} onChange={handleChangeTitle} placeholder="Название" />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default CreateModal

import api from '../../api'
import { AppStore } from '../../stores'

export function createFolder(title) {
    const folder = {
        userId: AppStore.user.id,
        title
    }
    
    return api.Folder.createFolder(folder)
}

export function createTask(name) {
    const task = {
        folderId: AppStore.activeFolder.id,
        name
    }
    
    return api.Task.createTask(task)
}
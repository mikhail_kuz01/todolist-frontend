import React from 'react'
import { Modal } from 'antd'
import { deleteFolder, deleteTask } from './hooks'
import { getFolders } from '../../Layout/hooks'
import { getTasksByFolder } from '../../pages/client/Folder/hooks'

const DeleteModal = (props) => {
    const { visible, onToggle, folder, id } = props

    const handleCloseModal = () => onToggle(false)

    const handleDelete = () => {
        if (folder) {
            deleteFolder(id).then(() => {
                getFolders()
            })
        } else {
            deleteTask(id).then(() => {
                getTasksByFolder()
            })
        }
        onToggle(false)
    }

    return (
        <Modal title={`Удаление ${folder ? 'папки' : 'задачи'}`} visible={visible} onOk={handleDelete} onCancel={handleCloseModal} okText="Да, удалить" cancelText="Отмена">
            <div>{`Вы действительно хотите удалить ${folder ? 'папку': 'задачу'}?`}</div>
        </Modal>
    )
}

export default DeleteModal

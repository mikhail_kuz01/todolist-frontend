import api from '../../api'

export function deleteFolder(id) {  
    return api.Folder.deleteFolder(id)
}

export function deleteTask(id) {
    return api.Task.deleteTask(id)
}
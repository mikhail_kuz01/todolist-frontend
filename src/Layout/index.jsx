import React from 'react'
import { Button, Layout, Menu, PageHeader } from 'antd'
import { DeleteOutlined, FolderAddOutlined, FolderOutlined, UnorderedListOutlined } from '@ant-design/icons'
import { Link, Redirect, Route, Switch, useHistory } from 'react-router-dom'
import { userIsLogged } from '../pages/base/Login/hooks'
import { useAppStore } from '../stores'
import { getFolders } from './hooks'
import { observer } from 'mobx-react'
import * as Client from '../pages/client'
import { CreateModal, DeleteModal } from '../components'


const { Sider } = Layout
const { Item } = Menu

const AppLayout = observer(
    () => {
        const AppStore = useAppStore()
        const history = useHistory()

        React.useEffect(() => {
            userIsLogged().then(({ data }) => {
                AppStore.setUser(data)
                getFolders().then(() => {
                    history.replace('/')
                })
            }).catch(() => {
                localStorage.clear()
                history.replace('/login')
            })
        // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [])

        const handleLogout = () => {
            history.replace("/login")
        }

        const [deleteFolder, setDeleteFolder] = React.useState(null)
        const [visibleCreateModal, setVisibleCreateModal] = React.useState(false)
        const [visibleDeleteModal, setVisibleDeleteModal] = React.useState(false)
        
        const handleToggleCreateModal = () => setVisibleCreateModal(true)
        const handleToggleDeleteModal = (id) => () => {
            setDeleteFolder(id)
            setVisibleDeleteModal(true)
        }

        const handleSetActiveFolder = (e) => {
            AppStore.setActiveFolders(AppStore.folders.find((folder) => folder.id === +e.key))
        }

        return (
            <Layout className="page">
                <Layout className="page-home">
                    <Sider width="300px" >
                        <Menu onClick={handleSetActiveFolder}>
                            <PageHeader 
                                className="home-title"
                                title="TODO List"
                                subTitle={`${AppStore.user.lastName} ${AppStore.user.firstName}`}
                                onBack={handleLogout}
                            />
                            {/* <Item key="all" icon={<UnorderedListOutlined />}>
                                <Link to="/all">
                                    Все задачи
                                </Link>
                            </Item> */}
                            {AppStore.folders.map((folder) => (
                                <Item key={folder.id} icon={<FolderOutlined />}>
                                    <Link to={`/folder/${folder.id}`}>
                                        {folder.title}
                                    </Link>
                                    <Button size="small" type="text" className="delete-btn" onClick={handleToggleDeleteModal(folder.id)}><DeleteOutlined /></Button>
                                </Item>
                            ))}
                            <Button className="create-folder-btn" type="text" onClick={handleToggleCreateModal}><FolderAddOutlined />Создать папку</Button>
                        </Menu>
                    </Sider>
                    <Layout>
                        <Switch>
                            {/* <Route path="/all">
                                <Client.FoldersList data={AppStore.folders} />
                            </Route> */}
                            <Route path="/folder">
                                <Client.Folder />
                            </Route>

                            {/* <Redirect from="/" to="/all" /> */}
                        </Switch>
                    </Layout>
                </Layout>
                {visibleCreateModal && <CreateModal visible={visibleCreateModal} onToggle={setVisibleCreateModal} folder />}
                {visibleDeleteModal && <DeleteModal visible={visibleDeleteModal} onToggle={setVisibleDeleteModal} folder id={deleteFolder} />}
            </Layout>
        )
    }
)

export default AppLayout

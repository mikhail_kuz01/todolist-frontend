import api from "../api"
import { AppStore } from "../stores"

export function getFolders() {
    return api.Folder.getFolderByUser(AppStore.user.id).then(({ data }) => {
        AppStore.setFolders(data)
    })
}
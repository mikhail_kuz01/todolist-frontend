import React from 'react'
import { Button, Form, Input, notification, PageHeader, Space } from 'antd'
import { SwapRightOutlined } from '@ant-design/icons'
import { Link, useHistory } from 'react-router-dom'
import { findUserByLogin, registerUser } from './hooks'

import './styles'
import { isNumber } from 'lodash'

const { Item } = Form

const Register = () => {
    const history = useHistory()
    const [form] = Form.useForm()

    const [rePassword, setRePassword] = React.useState('')
    const [user, setUser] = React.useState({
        userName: null,
        password: null,
        firstName: null,
        lastName: null,
    })

    const handleChangeData = (type) => (e) => {
        switch (type) {
            case 'userName':
            case 'password':
            case 'firstName':
            case 'lastName': {
                setUser({
                    ...user,
                    [type]: e.target.value
                })
                break
            }
            default: {
                setRePassword(e.target.value)
            }
        }
    }

    const handleRegister = () => {
        form.validateFields().then(() => {
            if (rePassword !== user.password)
                return notification.error({
                    message: 'Invalid Data!',
                    description: 'Пароли должны совпадать',
                    placement: 'bottomRight',
                    duration: 5
                })
            
            if (isNumber(+findUserByLogin(user.userName).then((data) => data))) {
                return notification.error({
                    message: 'Invalid Data!',
                    description: 'Пользователь с таким логином уже существует',
                    placement: 'bottomRight',
                    duration: 5
                })
            }
            
            registerUser(user).then(() => {
                history.replace('/login')
            })
        })
    }

    return (
        <Space className="page page-register" direction="vertical">
            <Form form={form} className="register-form">
                <Link to="/login" className="link-to">Авторизация<SwapRightOutlined /></Link>
                <PageHeader 
                    className="register-form-title"
                    title="TODO List"
                    subTitle="Регистрация"
                    
                />
                <div className="register-form-inputs">
                    <Item name="userName" rules={[{ required: true, message: 'Пожалуйста, введите логин' }]}>
                        <Input value={user.userName} onChange={handleChangeData('userName')} placeholder="Логин" />
                    </Item>
                    <Item name="firstName" rules={[{ required: true, message: 'Пожалуйста, введите ваше имя' }]}>
                        <Input value={user.firstName} onChange={handleChangeData('firstName')} placeholder="Имя" />
                    </Item>
                    <Item name="lastName" rules={[{ required: true, message: 'Пожалуйста, введите вашу фамилию' }]}>
                        <Input value={user.lastName} onChange={handleChangeData('lastName')} placeholder="Фамилия" />
                    </Item>
                    <Item name="password" rules={[{ required: true, message: 'Пожалуйста, введите пароль' }]}>
                        <Input value={user.password} onChange={handleChangeData('password')} placeholder="Пароль" />
                    </Item>
                    <Item name="rePassword" rules={[{ required: true, message: 'Пожалуйста, введите повторно пароль' }]}>
                        <Input value={rePassword} onChange={handleChangeData('rePassword')} placeholder="Повторный пароль" />
                    </Item>
                </div>
                <Button className="register-form-btn" key="submit" type="primary" onClick={handleRegister}>Зарегистрироваться</Button>
            </Form>
        </Space>
    )
}

export default Register

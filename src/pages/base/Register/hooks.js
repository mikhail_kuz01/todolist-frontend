import api from '../../../api'

export function registerUser(user) {
    return api.User.registerUser(user)
}

export function findUserByLogin(login) {
    return api.User.getUserByLogin(login).then(({ data }) => {
        return data
    })
}
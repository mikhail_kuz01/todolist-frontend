import React from 'react'
import { Button, Form, Input, notification, PageHeader, Space } from 'antd'
import { SwapRightOutlined } from '@ant-design/icons'
import { login, userIsLogged } from './hooks'
import { Link, useHistory } from 'react-router-dom'

import './styles'

const { Item } = Form

const Login = () => {
    const history = useHistory()
    const [form] = Form.useForm()

    const [authData, setAuthData] = React.useState({
        userName: '',
        password: ''
    })

    React.useEffect(() => {
        localStorage.clear()
        userIsLogged().then(() => {
            history.replace('/')
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleChangeLogin = (e) => setAuthData({
        ...authData,
        userName: e.target.value
    })
    
    const handleChangePassword = (e) => setAuthData({
        ...authData,
        password: e.target.value
    })

    const handleLogin = () => {
        form.validateFields().then(() => {
            login(authData).then(() => {
                history.replace('/')
            }).catch(() => {
                notification.error({
                    message: 'Invalid Data!',
                    description: 'Неверные логин или пароль, попробуйте еще раз',
                    placement: 'bottomRight',
                    duration: 5
                })
            })
            
            form.resetFields()
        })
    }

    return (
        <Space className="page page-login">
            <Form form={form} className="login-form">
                <Link to="/register" className="link-to">Регистрация<SwapRightOutlined /></Link>
                <PageHeader 
                    className="login-form-title"
                    title="TODO List"
                    subTitle="Авторизация"
                    
                />
                <div className="login-form-inputs">
                    <Item name="userName" rules={[{ required: true, message: 'Пожалуйста, введите ваш логин' }]}>
                        <Input value={authData.userName} onChange={handleChangeLogin} placeholder="Логин" />
                    </Item>
                    <Item name="password" rules={[{ required: true, message: 'Пожалуйста, введите пароль' }]}>
                        <Input value={authData.password} onChange={handleChangePassword} placeholder="Пароль" />
                    </Item>
                </div>
                <Button className="login-form-btn" key="submit" type="primary" onClick={handleLogin}>Войти</Button>
            </Form>
        </Space>
    )
}

export default Login

import api from '../../../api'

export function login(user) {
    return api.Authentication.userLogin(user).then(({ data }) => {
        localStorage.setItem('USER_KEY', data.token)
    })
}

export function userIsLogged() {
    return api.Authentication.fetchUserData()
}
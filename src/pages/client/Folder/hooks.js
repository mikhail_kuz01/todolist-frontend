import api from '../../../api'
import { AppStore } from '../../../stores'

export function getTasksByFolder(folder, id) {
    const _folder = folder || AppStore.activeFolder
    const _id = id || AppStore.activeFolder.id

    api.Task.getTasksByFolder(_id).then(({ data }) => {
        AppStore.setActiveFolders({
            ..._folder,
            tasks: data
        })
    })
}

export function restoreTask(task) {
    return api.Task.createTask(task)
}
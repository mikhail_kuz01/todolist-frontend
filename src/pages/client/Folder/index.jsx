import React from 'react'
import { Button, Divider, List, PageHeader, Space } from 'antd'
import { useHistory } from 'react-router-dom'
import { getTasksByFolder, restoreTask } from './hooks'
import { useAppStore } from '../../../stores'
import { CheckSquareFilled, CheckSquareOutlined, DeleteOutlined, FileAddOutlined } from '@ant-design/icons'
import { observer } from 'mobx-react'
import { isEmpty } from 'lodash'
import { Content } from 'antd/lib/layout/layout'
import { CreateModal, DeleteModal } from '../../../components'
import { getFolders } from '../../../Layout/hooks'

import './styles'

const Folder = observer(
    () => {
        const AppStore = useAppStore()
        const { location } = useHistory()
        const { activeFolder } = useAppStore()

        React.useEffect(() => {
            getFolders()
            const folder = AppStore.folders.find(folder => folder.id === +location.pathname.split('/')[2])
            getTasksByFolder(folder, folder?.id)
        // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [location.pathname])

        const [visibleCreateModal, setVisibleCreateModal] = React.useState(false)
        const [visibleDeleteModal, setVisibleDeleteModal] = React.useState(false)
        const [deleteTask, setDeleteTask] = React.useState(null)

        const handleCreateTask = () => setVisibleCreateModal(true)
        const handleDeleteTask = (id) => () => {
            setDeleteTask(id)
            setVisibleDeleteModal(true)
        }

        const handleDoneTask = (task) => () => {
            restoreTask({
                ...task,
                done: !task.done
            }).then(() => {
                getTasksByFolder()
            })
        } 

        return (
            <Space direction="vertical">
                <PageHeader title={activeFolder.title} />
                <Divider style={{ margin: '0 24px'}} />
                <Content className="page-content">
                    {!isEmpty(activeFolder.tasks) && 
                        <div className="tasks_list">
                            <List
                                dataSource={activeFolder.tasks} 
                                renderItem={(item) => (
                                    <List.Item className="task">
                                        <div>
                                            <Button type="text" onClick={handleDoneTask(item)}>{item.done ? <CheckSquareFilled style={{ color: '#1890ff' }} /> : <CheckSquareOutlined />}</Button>
                                            {item.name}
                                        </div>
                                        <Button className="delete-btn" type="text" onClick={handleDeleteTask(item.id)}><DeleteOutlined /></Button>
                                    </List.Item>
                                )}
                            />
                        </div>
                    }
                    <Button type="primary" onClick={handleCreateTask}><FileAddOutlined />Создать задачу</Button>
                </Content>
                {visibleCreateModal && <CreateModal visible={visibleCreateModal} onToggle={setVisibleCreateModal} />}
                {visibleDeleteModal && <DeleteModal visible={visibleDeleteModal} onToggle={setVisibleDeleteModal} id={deleteTask} />}
            </Space>
        )
    }
)

export default Folder

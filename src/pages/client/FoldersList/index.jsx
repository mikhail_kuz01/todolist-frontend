import { CheckSquareFilled, CheckSquareOutlined } from '@ant-design/icons'
import { Button, Divider, List, PageHeader } from 'antd'
import { Content } from 'antd/lib/layout/layout'
import { observer } from 'mobx-react'
import React from 'react'
import { useAppStore } from '../../../stores'
import { getTasks } from './hooks'

const FoldersList = observer(
    (props) => {
        const {data} = props
        const AppStore = useAppStore()
        
        React.useEffect(() => {
            getTasks(data.map((i) => i.id))
        }, [data])

        console.log(AppStore.tasks)

        return (
            <div>
                {data.map((folder) => {
                    return (
                        <div>
                            <PageHeader key={`page-header-folder-${folder.id}`} title={folder.title} />
                            <Divider key={`divider-folder-${folder.id}`} style={{ margin: '0 24px'}} />
                            <Content key={`content-folder-${folder.id}`} className="page-content">
                                {(
                                    <List 
                                        key={`list-folder-${folder.id}`}
                                        dataSource={AppStore.tasks[folder.id]}
                                        renderItem={(task) => (
                                            <List.Item key={`list-folder-${folder.id}-task-${task.id}`}>
                                                <Button key={`button-folder-${folder.id}-task-${task.id}`} type="text">{task.done ? 
                                                    <CheckSquareFilled key={`icon1-folder-${folder.id}-task-${task.id}`} style={{ color: '#1890ff' }} /> : 
                                                    <CheckSquareOutlined key={`icon1-folder-${folder.id}-task-${task.id}`} />
                                                }</Button>
                                                {task.name}
                                            </List.Item>
                                        )}
                                    />
                                )}
                            </Content>
                        </div> 
                    )
                })}
            </div>
        )
    }
)


export default FoldersList

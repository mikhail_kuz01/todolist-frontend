import api from '../../../api'
import { AppStore } from '../../../stores';

export function getTasks(folderIds) {
    const tasks = new Map()
    
    folderIds.map((id) => {
        return api.Task.getTasksByFolder(id).then(({ data }) => {
            tasks.set(id, data)
        })    
    })

    AppStore.setTasks(tasks)

    return tasks;
}
import { useContext, createContext, createElement } from 'react'
import { action, makeAutoObservable, observable } from 'mobx'

export class MainStore {
    user = {}
    activeFolder = {}
    folders = []
    tasks

    constructor() {
        makeAutoObservable(this, {
            user: observable,
            folders: observable,
            tasks: observable,
            activeFolder: observable,
            setUser: action,
            setFolders: action,
            setTasks: action,
            setActiveFolder: action
        })
    }

    setUser(user) {
        this.user = user
    }
    
    setFolders(folders) {
        this.folders = folders
    }

    setActiveFolders(folder) {
        this.activeFolder = folder
    }

    setTasks(tasks) {
        this.tasks = tasks
    }
}

export const AppStore = new MainStore()
const AppStoreContext = createContext(AppStore)

export const useAppStore = () => useContext(AppStoreContext)
export const AppStoreProvider = ({ children }) => createElement(AppStoreContext.Provider, { children, value: AppStore })
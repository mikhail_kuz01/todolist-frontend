import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { AppStoreProvider } from './stores'

import * as Base from './pages/base'
import AppLayout from './Layout'

const App = () => {
  return (
    <AppStoreProvider>
      <Router>
        <Switch>
          <Route path="/login" component={Base.Login} />
          <Route path="/register" component={Base.Register} />

          <Route path="/" component={AppLayout} />
        </Switch>
      </Router>
    </AppStoreProvider>
  )
}

export default App

import AuthenticationService from './Authentication.service'
import FolderService from './Folder.service'
import TaskService from './Task.service'
import UserService from './User.service'

class Services {
    
    Authentication = new AuthenticationService()
    User = new UserService()
    Folder = new FolderService()
    Task = new TaskService()
}

export const BaseURL = 'http://localhost:8080'

export function getToken() {
    return localStorage.getItem('USER_KEY')
}

export default Services
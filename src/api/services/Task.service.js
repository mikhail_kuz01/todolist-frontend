import axios from 'axios'
import { BaseURL, getToken } from '.'

export class TaskService {

    getTasksByFolder(id) {
        return axios.get(process.env.hostUrl || `${BaseURL}/api/task/folder/` + id, {
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }

    createTask(task) {
        return axios.post(process.env.hostUrl || `${BaseURL}/api/task/create`, task, { 
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }

    deleteTask(id) {
        return axios.delete(process.env.hostUrl || `${BaseURL}/api/task/delete/` + id, { 
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }
}

export default TaskService
import axios from 'axios'
import { BaseURL, getToken } from '.'

export class FolderService {

    getFolderByUser(id) {
        return axios.get(process.env.hostUrl || `${BaseURL}/api/folder/user/` + id, {
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }

    createFolder(folder) {
        return axios.post(process.env.hostUrl || `${BaseURL}/api/folder/create`, folder, { 
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }

    deleteFolder(id) {
        return axios.delete(process.env.hostUrl || `${BaseURL}/api/folder/delete/` + id, { 
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }
}

export default FolderService
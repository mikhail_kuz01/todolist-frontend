import axios from 'axios'
import { BaseURL } from '.'

export class UserService {

    getUserByLogin(login) {
        return axios.get(process.env.hostUrl || `${BaseURL}/api/user/find/` + login)
    }

    registerUser(user) {
        return axios.post(process.env.hostUrl || `${BaseURL}/api/user/create`, user)
    }
}

export default UserService
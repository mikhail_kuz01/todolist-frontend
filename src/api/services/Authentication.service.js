import axios from 'axios'
import { BaseURL, getToken } from '.'

export class AuthenticationService {

    userLogin(authRequest) {
        return axios.post(process.env.hostUrl || `${BaseURL}/api/auth/login`, authRequest)
    }

    fetchUserData(authRequest) {
        return axios.get(process.env.hostUrl || `${BaseURL}/api/auth/userinfo`, {
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
    }
}

export default AuthenticationService
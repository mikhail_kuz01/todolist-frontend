import Services from './services'

class API extends Services { }

export const api = new API()
export default api
export { API }